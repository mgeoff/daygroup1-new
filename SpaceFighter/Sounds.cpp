#include "Sounds.h"
#include <Windows.h>
#include <mmsystem.h>

void Sounds::MenuMusic()
{
	PlaySound(TEXT("song.wav"), NULL, SND_ASYNC);
}

void Sounds::BlasterSound()
{
	PlaySound(TEXT("pew.wav"), NULL, SND_ASYNC);
}

void Sounds::CollisionSound()
{
	PlaySound(TEXT("bang.wav"), NULL, SND_ASYNC);
}
void Sounds::SoundOff()
{
	PlaySound(NULL, NULL, NULL);
}
