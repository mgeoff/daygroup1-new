#pragma once
#include "GameObject.h"
#include "Weapon.h"
class Explosion :
    public GameObject
{
public:
    virtual void LoadContent(ResourceManager* pResourceManager);

    virtual void Draw(SpriteBatch* pSpriteBatch);

private:

    Texture m_sExplode;



};

